$(document).ready(function()
{

    $("#animacion1").click(function(){
     $( "#resultado1" ).toggleClass( "show  ", 2000 );     
    });
    $("#animacion2").click(function(){
        $( "#resultado2" ).toggleClass( "show  ", 2000 );     
       });
       $("#animacion3").click(function(){
        $( "#resultado3" ).toggleClass( "show  ", 2000 );     
       });




       var Ejercicio =[
        {
       numeral: 1,
          tipo: 'directo',
          enunciado :'Al llegar al hotel nos han dado un mapa con los lugares de interés de la ciudad, y nos han dicho que 5 centímetros del mapa representan 600 metros de la realidad. Hoy queremos ir a un parque que se encuentra a 8 centímetros del hotel en el mapa. ¿A qué distancia del hotel se encuentra este parque?',
           
         
          resultado : '960 metros',
          incognita: 960,
           opA: 950,
           opD: 960,
           opC: 970,
           opB: 980,
           varA: "600 metros",
           varB: "8 centimetros",
           varC: "5 centimetros",
           resM: "4800 metros",
           final:"Como resultado podemos decir que el hotel esta a  <strong> 960 metros </strong>"

         
       },


       {
           numeral: 2,
              tipo: 'directo',
              enunciado :'Un automóvil recorre  240  km en  3  horas. ¿Cuántos kilómetros habrá recorrido en  2  horas?',
              
               incognita: 160,
              resultado: '160 kilometros',
              opA: 150,
              opB: 140,
              opC: 160,
              opD: 170,
              varA: "240 km",
              resM: "480 km",
              varB: "2 horas",
              varC: "3 horas",
              final: "Como resultado el automovil recorrio <strong> 160 Km </strong> en 2 Horas"
       },

       {
           numeral: 3,
              tipo: 'directo',
              enunciado :'Ana compra 5 kg de papas, si 2 kg cuestan 1800 $, ¿cuánto pagará Ana?',
           
              incognita: 4500,
              resultado: '4500 $',
              opA: 4500,
              opB: 3600,
              opC: 5400,
              opD: 4000,
              varA: "1800 $",
              varB: "5 kg",
              varC: "2 kg",
              resM: "9000 $",
              final: "Como resultado los 5kg de papas costaron   <strong> 4500$ </strong>  "
       },
       // {
       //     numeral: 4,
       //        tipo: 'directo',
       //        enunciado :'Un automóvil recorre  140  km en  3  horas. ¿Cuántos kilómetros habrá recorrido en  2  horas?',
       //         varA: 140,
       //         varB: 3,
       //         varC: 2,
       //        incognita: 93,
       //        resultado: '93 kilometros'

       // },
    

] ;
Ejercicio.forEach((item,index)=>{
     // <input type="button" value="ver proceso" id="proceso${item.numeral}" />
    var ejercicio =`
    <article Class ="ejercicioD">
    <h2>Ejercicio N° ${item.numeral} </h2>
    <p> ${item.enunciado} </p>

    <fieldset>
    <legend>Elige la respuesta correcta</legend>
    <label>
    <input type="radio"  name="E_${item.numeral}" value=" ${item.opA}"> A.${item.opA}
    </label>
    <br>
    <label>
    <input type="radio" name="E_${item.numeral}" value=" ${item.opB}"> B.${item.opB}
    </label>  <br>
    <label>
    <input type="radio" name="E_${item.numeral}" value="${item.opC}" > C.${item.opC}
    </label>  <br>
    <label>
    <input type="radio" name="E_${item.numeral}" value="${item.opD}"> D.${item.opD}
    </label>
    </fieldset>
    
    
    
    
    <input type="button" value="Guardar respuesta " id="${item.numeral}"  Class ="miboton" >
    <a href="#proceso${item.numeral}" rel="modal:open" class=" emergente  boton2  ocultar  Ejercicio"  >Ver proceso</a>
    
    
    <div id="proceso${item.numeral}" class="modal"  >

    <div class="titulo">Ejercicio ${item.numeral} </div>
    <div class=" popup">

   



      <ol> 
          <li><p>Multiplicar: <br>${item.varA}  x ${item.varC}  = <strong>${item.resM}</strong></p> </li>
          <li><p> A <strong>${item.resM}</strong> dividirlo en ${item.varC} : <br> ${item.resM}/${item.varC} = <strong>${item.resultado}</strong></p></li>
          <li><p> ${item.final} </p></li>
      </ol>
      

  </div>

  <img src="../img/gato3.jpg" width="200"> 
   
    </div>
    </div>
   

    <br>
    <br>
    <hr>
</article>
    `;


    
    //document.getElementById("ED").innerHTML += (ejercicio);
   // console.log (ejercicio);
     $('#post').append(ejercicio);
  
});




var acum=0;
    
function manejadorCallback(evento) {


    
    var index =parseInt(evento.target.id) ;
   
    const indexado = Ejercicio.reduce((acc,el)=>  ({
     ...acc,
     [el.numeral] :el,  
    }),{});

     var incog = indexado[index].incognita ;
    
    
  
   
    
     var memo=document.getElementsByName("E_"+index);
     for(i=0; i<memo.length; i++){
       
         if(memo[i].checked){
             var memory=memo[i].value;

           //  alert(memory);
          
             if(incog ==memory){
                 acum+=1;
                // alert ("Respuesta Correcta");
            }
            else{
                // alert ("Respuesta Erronea");
            }

         } 
         else{
             memo [i].disabled =true;
         }
       
    } console.log( acum);
  
    
 }
  


 $("#total").click(function (e) { 
    person = prompt("Ingresa tu nombre y apellido");
   e.preventDefault();
   $(".Ejercicio").removeClass("ocultar");
   $(".Ejercicio").addClass("mostrar");
   $('#total').attr("disabled", true);
   
});
  
 var person;

function calificacion( ){

 let preguntas =Ejercicio.length;

 alert( "Nombre: "+person+ "\r"+"Su calificación es: "+(5/preguntas *acum));


}





  var buttons = document.querySelectorAll('.miboton')
  for(var i = 0; i < buttons.length; i++) {

    // aqui generas el equivalente a onclick

   
    buttons[i].addEventListener('click',  manejadorCallback )
     
    };



  
 var total= document.querySelector("#total");

 total.addEventListener('click' , calificacion);









    

    

    

});